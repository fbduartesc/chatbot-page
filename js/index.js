import '../sass/index.scss';
import pathToAvatarA from '../images/user-avatar.png';
import pathToAvatarB from '../images/robot.png';

let chatForm = document.querySelectorAll('.chat-input-area');
let chatInputField = document.querySelectorAll('.chat-input-area__input');
let chatLog = document.querySelectorAll('.chat-log');

let loading = false;

/**
 * Scrolls the contents of a container to the bottom
 * @function scrollContents 
*/
function scrollContents(container) {
  container.scrollTop = container.scrollHeight;
}

/**
 * Create a DOM element from a string value
 * @function getMessageElement 
 * @param {string} val
 * @param {boolean} isUser
 * @returns {HTMLElement}
*/
function getMessageElement(val, isUser) {  
  let newMessage = document.createElement('div');

  if (isUser) {
    newMessage.className += 'chat-message--right ';
  }
  
  let avatarFrame = document.createElement('span');
  avatarFrame.className += 'chat-message__avatar-frame';
  let avatar = document.createElement('img');
  avatar.src = isUser ? pathToAvatarA : pathToAvatarB;
  avatar.alt = 'avatar';
  avatar.className += 'chat-message__avatar';

  // Create text
  let text = document.createElement('p');  
  text.insertAdjacentHTML('beforeend', val);
  text.className += 'chat-message__text';

  // Append elements
  avatarFrame.append(avatar);
  newMessage.append(avatarFrame);
  newMessage.append(text);  
  newMessage.className += 'chat-message ';

  return newMessage;
}

/**
 * Requests a reply
 * @function getReply 
 * @param {string} input
*/
function getReply(input) {
  loading = true;

  const options2 = {
    method: 'POST',
    mode: 'cors',
    headers: new Headers({
      'Content-Type': 'application/json; charset=utf-8'
    }),
    body: JSON.stringify({
      'message': input
    })
  };

  fetch(`${process.env.URL_API}`, options2)
    .then(function (response) {
      return response.json();
    })
    .then(onGetReplySuccess)
    .catch(onGetReplyError)
}

/**
 * Handles what happens when getReply succeeds
 * @function onGetReplySuccess 
 * @param {Object|String} data
*/
function onGetReplySuccess(data) {  
  data = data.message;
  let newMessage = typeof data === 'string' ?
    getMessageElement(data, false) :
    getMessageElement(data.result.fulfillment.speech, false);

  // Scroll to last message
  chatLog[0].append(newMessage);
  scrollContents(chatLog[0]);
  loading = false;
}

/**
 * Handles what happens when getReply fails
 * @function onGetReplyError 
*/
function onGetReplyError() {
  let newMessage = getMessageElement('Oh não, ocorreu um erro!');

  // Scroll to last message
  chatLog[0].append(newMessage);
  scrollContents(chatLog[0]);
  loading = false;
}

// Handle form submit (clicking on the submit button or pressing Enter)
chatForm[0].addEventListener('submit', function (e) {
  e.preventDefault();

  // If reply is loading, wait
  if (loading) {
    return false;
  }

  // Catch empty messages
  if (!chatInputField[0].value) {
    return false;
  }

  // Add user's message to the chat log
  let newMessage = getMessageElement(chatInputField[0].value, true);
  chatLog[0].append(newMessage);

  // Scroll to last message
  scrollContents(chatLog[0]);

  // Get the reply from dialogflow.com
  getReply(chatInputField[0].value);

  // Clear input
  chatInputField[0].value = '';
});