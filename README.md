<div align="center">
  <img alt="Chatbot" src="images/chatbot.png" height="400px" />    
  <h1>HTML5 | CSS3 | SASS | Javascript</h1>

 :rocket: *Chatbot page: this project will build a bot for telegram using javascript, HTML5, CSS3 and SASS*
  </div>

# :robot: Chatbot page
Demo: [Chatbot page](https://fbduartesc.gitlab.io/chatbot-page)

# :pushpin: Table of contents

- [Technologies](#computer-technologies)
- [How to run](#construction_worker-how-to-run)
- [License](#closed_book-license)

# :computer: Technologies

This project was made using the following technologies:

<ul>
  <li><a href="https://www.w3schools.com/html/">HTML5</a></li>
  <li><a href="https://developer.mozilla.org/pt-BR/docs/Web/CSS">CSS3</a></li>
  <li><a href="https://developer.mozilla.org/pt-BR/docs/Web/JavaScript">Javascript</a></li>
  <li><a href="https://sass-lang.com/guide">SASS</a></li>
</ul>

# :construction_worker: How to run

### :computer: Instructions 
```bash
# Rename the file .env.sample to .env
# Change the variable value URL_API to http://localhost:5000
```
### :computer: Ngrok
Installation is simple. Just [download](https://ngrok.com/download) the file and follow the instructions on the page.
```bash
# Once installed, just run the command
$ ./ngrok http 1234
# This is the port that is running our web server.
```

### :robot: Chatbot API Project

To run correctly it is necessary to start the [Chatbot API](https://gitlab.com/fbduartesc/telegram-chatbot-api).

### :computer: Downloading project 

```bash
# Clone repository into your machine
$ git clone https://gitlab.com/fbduartesc/chatbot-page.git
```

### :computer: Installation 

```bash
# Install dependencies
$ npm install
```

### 💻 Running project on a web browser

```bash
# Run the command, to start the application
$ npm run dev
```

# :closed_book: License

Released in 2020.

Made with passion by [Fabio Duarte de Souza](https://gitlab.com/fbduartesc) 🚀.
This project is under the [MIT license](https://gitlab.com/fbduartesc/chatbot-page/blob/master/LICENSE).